(define-module (osef)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (guix search-paths)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages tor)
  #:use-module (gnu packages chromium)
  #:use-module (gnu packages tmux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages web)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages virtualization)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-compression)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-check)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module ((guix licenses) :prefix license:))

                                        ; daemux
                                        ; selenium
                                        ; websockets

(define-public python-wsproto-1
  (package
   (name "python-wsproto")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "wsproto" version))
     (sha256
      (base32 "0f4cghvmjiljgw5jjnqvfn9r3gkbfhmk5xr0jw6sv5v98pw7d1w6"))))
   (build-system python-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     ;; (add-after 'wrap 'wtf
                     ;;            (lambda _
                     ;;              (invoke "echo" "WTF")
                     ;;              #t))
                     (delete 'check))))
   (propagated-inputs `(("python-h11" ,python-h11)))
   (home-page "https://github.com/python-hyper/wsproto/")
   (synopsis "WebSockets state-machine based protocol implementation")
   (description "WebSockets state-machine based protocol implementation")
   (license license:expat)))


(define-public python-sortedcontainers-2.4
  (package
   (name "python-sortedcontainers")
   (version "2.4.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "sortedcontainers" version))
     (sha256
      (base32 "126vpywl7aly6zir033a9indgyficlzl68qls61nn2y3djhabji5"))))
   (build-system python-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (delete 'check))))
   (home-page "http://www.grantjenks.com/docs/sortedcontainers/")
   (synopsis "Sorted Containers -- Sorted List, Sorted Dict, Sorted Set")
   (description "Sorted Containers -- Sorted List, Sorted Dict, Sorted Set")
   (license license:asl2.0)))

(define-public python-ruamel.yaml-0.16
  (package
   (name "python-ruamel.yaml")
   (version "0.16.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "ruamel.yaml" version))
     (sha256
      (base32
       "0jjkv2vym8nyf3dhwpfyi06rsymwgvlwm862hy2ymdch4z9bl7ac"))))
   (build-system python-build-system)
   (arguments
    `(
      #:tests? #f))
   (home-page "https://bitbucket.org/ruamel/yaml")
   (synopsis "YAML 1.2 parser/emitter")
   (description
    "This package provides YAML parser/emitter that supports roundtrip
preservation of comments, seq/map flow style, and map key order.  It
is a derivative of Kirill Simonov’s PyYAML 3.11.  It supports YAML 1.2
and has round-trip loaders and dumpers.  It supports comments.  Block
style and key ordering are kept, so you can diff the source.")
   (license license:expat)))

(define-public python-protobuf-3.14
  (package
   (name "python-protobuf")
   (version "3.14.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "protobuf" version))
     (sha256
      (base32
       "1kpq44zg664sic8n0jiaaammsyqwjphbwizv2n4ksaa7jcwfnqqx"))))
   (build-system python-build-system)
   (native-inputs
    `(("python-wheel" ,python-wheel)))
   (propagated-inputs
    `(("python-six" ,python-six)))
   (home-page "https://github.com/google/protobuf")
   (synopsis "Protocol buffers is a data interchange format")
   (description
    "Protocol buffers are a language-neutral, platform-neutral extensible
mechanism for serializing structured data.")
   (license license:bsd-3)))

(define-public python-ldap3-2.8
  (package
    (name "python-ldap3")
    (version "2.8")
    (home-page "https://github.com/cannatag/ldap3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference (url home-page)
                           (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "12yyrz1hr7j14i65hrzbppmcfcwjvwnnjcpwh3ylqsqdsxd13d8g"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f ;TODO: Tests need a real LDAP server to run
       #:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (when tests?
                        (invoke "nosetests" "-s" "test"))
                      #t)))))
    (native-inputs
     `(("python-nose" ,python-nose)))
    (propagated-inputs
     `(("python-gssapi" ,python-gssapi)
       ("python-pyasn1" ,python-pyasn1)))
    (synopsis "Python LDAP client")
    (description
     "LDAP3 is a strictly RFC 4510 conforming LDAP V3 pure Python client
library.")
    (license license:lgpl3+)))

(define-public python-kaitaistruct
  (package
   (name "python-kaitaistruct")
   (version "0.9")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "kaitaistruct" version))
     (sha256
      (base32
       "1b97sfqqz34rq05mii29xr88h0qbayyi3k3r8d8db968gs0lan1x"))))
   (build-system python-build-system)
   (home-page "http://kaitai.io")
   (synopsis
    "Kaitai Struct declarative parser generator for binary data: runtime library for Python")
   (description
    "Kaitai Struct declarative parser generator for binary data: runtime library for Python")
   (license license:expat)))

(define-public python-mitmproxy
  (package
   (name "python-mitmproxy")
   (version "7.0.4")
   (source
    (origin
     (method url-fetch)
     (uri "https://github.com/mitmproxy/mitmproxy/archive/v7.0.4.tar.gz")
     (sha256
      (base32
       "18icxm4cymnkw4lcaq0b1wgsbvifdzsapkgbmi1h0gq5d66d2a47"))))
   (build-system python-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (delete 'check))))
   (propagated-inputs
    `(
      ("python-zstandard" ,python-zstandard)
      ("python-publicsuffix" ,python-publicsuffix2)
      ("python-wsproto" ,python-wsproto-1)
      ("python-urwid" ,python-urwid)
      ("python-tornado" ,python-tornado-6)
      ("python-sortedcontainers" ,python-sortedcontainers-2.4)
      ("python-ruamel.yaml" ,python-ruamel.yaml-0.16)
      ("python-pyperclip" ,python-pyperclip)
      ("python-pyparsing" ,python-pyparsing)
      ;; ("python-pyopenssl" ,python-pyopenssl)
      ;; ("python-pyasn1" ,python-pyasn1)
      ("python-protobuf" ,python-protobuf-3.14)
      ("python-passlib" ,python-passlib)
      ("python-msgpack" ,python-msgpack)
      ("python-ldap3" ,python-ldap3-2.8)
      ("python-kaitaistruct" ,python-kaitaistruct)
      ("python-hyperframe" ,python-hyperframe)
      ("python-h2" ,python-h2)
      ("python-flask" ,python-flask)
      ("python-brotli" ,python-brotli)
      ("python-blinker" ,python-blinker)
      ("python-asgiref" ,python-asgiref)
      ;; ("python-hpack" ,python-hpack)
      ;; ;;("python-hpack" ,python-hpack-4)
      ;; ;;("python-h2" ,python-h2-4)
      ;; ;;("python-hyperframe" ,python-hyperframe-6)
      ;; ;;("python-ldap3" ,python-ldap3-2.8)
      ;; ;;("python-ruamel.yaml" ,python-ruamel.yaml-16)
      ))
   (home-page "https://mitmproxy.org/")
   (synopsis
    "OSEF")
   (description
    "OSEF")
   (license license:bsd-3)))

(define-public python-questionary
  (package
   (name "python-questionary")
   (version "1.10.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "questionary" version))
     (sha256
      (base32 "143dji1lyb193vfvcfjcjdzw4jvfnvykdsgfjy6x89nfxkpkl3b0"))))
   (build-system python-build-system)
   (propagated-inputs
    `(
      ("python-prompt-toolkit" ,python-prompt-toolkit-2)
      ))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (delete 'check))))
   (home-page "https://github.com/tmbo/questionary")
   (synopsis "Python library to build pretty command line user prompts ??")
   (description "Python library to build pretty command line user prompts ??")
   (license license:expat)))

(define-public python-daemux
  (package
    (name "python-daemux")
    (version "0.1.0")
    (source
     ;; We fetch from the Git repo because there are no tests in the PyPI
     ;; archive.
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/edouardklein/daemux")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0cb8v552f2hkwz6d3hwsmrz3gd28jikga3lcc3r1zlw8ra7804ph"))))
    (build-system python-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda _
                      (mkdir-p "tmptmux")
                      (setenv "TMUX_TMPDIR" (string-append (getcwd) "/tmptmux"))
                      (invoke "tmux" "new-session" "-d")
                      (invoke "make" "test"))))))
    (propagated-inputs
     (list python-libtmux))
    (native-inputs
     (list python-coverage python-sphinx tmux python-flake8))
    (home-page "https://github.com/edouardklein/daemux")
    (synopsis "Start, stop, restart and check daemons via tmux")
    (description
     "Daemux lets you run daemons in a @command{tmux} pane.  Users can launch
long-running background tasks, and check these tasks' health by hand, relaunch
them, etc., by attaching to the corresponding pane in tmux.")
    (license license:agpl3+)))

(define-public python-transient
  (package
    (name "python-transient")
    (version "0.25")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "transient" version))
       (sha256
        (base32
         "1hi0n0mmqhsn9md46fmkjal6dfs0hq2f7ghmalg7hmwnq99ib5r8"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f ; Requires behave
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'fix-dependencies
                    (lambda _
                      (substitute* "setup.py"
                        (("==")
                         ">="))
                      (substitute* "setup.py"
                        (("~=")
                         ">="))
                      #t)))))
    (propagated-inputs
     (list python-beautifultable
           python-click
           python-importlib-resources
           python-lark-parser
           python-marshmallow
           python-progressbar2
           python-requests
           python-toml
           qemu))
    (native-inputs
     (list python-black python-mypy python-pyhamcrest python-twine))
    (home-page
     "https://github.com/ALSchwalm/transient")
    (synopsis
     "QEMU Wrapper written in Python")
    (description
     "@code{transient} is a wrapper for QEMU allowing the creation of virtual
machines with shared folder, ssh, and disk creation support.")
    (license license:expat)))

(define-public xscraper
  (package
   (name "xscraper")
    (version "1.0.5")
    (source
     (local-file "../xscr" #:recursive? #t))
    (build-system copy-build-system)
    (arguments
     `(
       #:install-plan
       `(("xscr" "bin/")
         ("xscraper"
          ,(string-append
            "lib/python"
            ,(version-major+minor
             (package-version python))
            "/site-packages/")))
           ))
    (propagated-inputs
     `(
       ;; ("make" ,gnu-make)
       ("docopt" ,python-docopt)
       ("python-networkx" ,python-networkx)
       ("python-beautifulsoup" ,python-beautifulsoup4)
       ("ipython" ,python-ipython)
       ;; ("clint" ,python-clint)
       ("python-questionary" ,python-questionary)
       ;; ("python-lxml" ,python-lxml)
       ("python-selenium" ,python-selenium)
       ("python-daemux" ,python-daemux)
       ;; ("flask" ,python-flask)
       ;; ("flask" ,python-flask-restx)
       ("python-random-user-agent" ,python-random-user-agent)
       ("python-libtmux" ,python-libtmux)
       ;; ("figlet" ,python-pyfiglet)
       ("python" ,python)
       ("python-mitmproxy" ,python-mitmproxy)
       ("tor" ,tor)
       ("uft8" ,glibc-utf8-locales)
       ("polipo" ,polipo)
       ("tmux" ,tmux)
       ("ungoogled-chromium" ,ungoogled-chromium)
       ("nss-certs" ,nss-certs)
       ;; ("coreutils" ,coreutils)
       ;; ("websockets" ,python-websockets)
       )
     )
    (native-search-paths
     (list
      (search-path-specification
       (variable "GUIX_LOCPATH")
       (separator #f)  ; single entry
       (files '("lib/locale")))))
    (synopsis "Web and tor scraper and crawler")
    (description "See documentation")
    (home-page "https://gitlab.com/edouardklein/xscr")
    (license license:agpl3+)))

xscraper

