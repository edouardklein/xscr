"""Signal-handling functions
--------------------------------"""

import xscraper as xs
import xscraper.scram as scram
import signal
from functools import partial
from xscraper.default_callbacks import console_once


def log_signal(signal, action):
    """Log the received signal on the console."""
    xs.LOGGER.warning("Signal {} has just been {}.".format(signal, action))


def sigint_handler(arguments, signal, _):
    """Quit everything instantly."""
    log_signal(signal, 'received')
    scram.dump_graph(emergency=True)
    scram.dump_form_value()
    scram.shutdown(emergency=True, arg=arguments)


def sigusr1_handler(signal, _):
    """Return to the console when receiving signal USR1"""
    log_signal(signal, 'received')
    with xs.LOCK:
        xs.CALLBACKS.insert(xs.CALLBACKS_INDEX + 1, console_once)
    log_signal(signal, 'handled')


def sigterm_handler(arguments, signal, _):
    """Quit everything (including TOR) cleanly"""
    log_signal(signal, 'received')

    def exit_f():
        """Call shutdown."""
        scram.dump_graph(emergency=False)
        scram.dump_form_value()
        scram.shutdown(arg=arguments)

    with xs.LOCK:
        xs.CALLBACKS.insert(xs.CALLBACKS_INDEX + 1, exit_f)
    log_signal(signal, 'handled')


def install_signals(arguments):
    """Installation of signal handlers for SIGINT, SIGTERM and SIGUSR1"""
    signal.signal(signal.SIGUSR1, sigusr1_handler)
    signal.signal(signal.SIGINT, partial(sigint_handler, arguments))
    signal.signal(signal.SIGTERM, partial(sigterm_handler, arguments))
