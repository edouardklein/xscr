"""Graph
----------

Handle scraping graph :py:data:`__init__.G` management"""

from bs4 import BeautifulSoup
from enum import Enum
import xscraper as xs


class Seen(Enum):
    """Describe the different states a node can be in w.r.t. having been
    scraped"""

    YET_TO_BE_SEEN = 0
    """The node has not been scraped yet, but should be"""
    SEEN = 1
    """The node has been scraped"""
    NOT_TO_BE_SEEN = -1
    """The node has not been scraped yet, and shouldn't be"""


def add_url(
    url="",
    parent_url=None,
    root=False,
    priority=0,
    seen=Seen.YET_TO_BE_SEEN,
    weight=1,
    **kwargs
):
    """Add url as a node to the global graph :py:data:`__init__.G`, and an
    edge from parent_url to url.

    Create the node in the graph.
    If the url already exists, the keyword arguments are ignored and the
    existing node is not
    changed. An edge is added, though.

    :param url: url of the webpage
    :param parent_url: url of a webpage that links to url
    :param root: indicate if the node is an origin node for Graph algorithm\
    like shortest_path_length
    :param priority: indicate the priority of the node. Node with the higher\
    value will be seen first
    :param seen: The node's visitation state
    :param weight: The weight edge value between the current and the parent node.
    :param kwargs: any additional key-value pairs to add to the node
    :type url: str
    :type parent_url: str
    :type root: bool
    :type priority: int
    :type seen: Seen object
    """
    # Checking if the page is not already represented in a node
    if url not in xs.G.nodes:
        xs.G.add_node(url, seen=seen, priority=priority, root=root, **kwargs)
    # Creating an edge between the new node and its optional parent node
    if parent_url:
        xs.G.add_edge(parent_url, url, weignt=weight)


def update_current():
    """Update :py:data:`__init__.CURRENT_SOUP`"""
    xs.CURRENT_SOUP = BeautifulSoup(xs.DRIVER.page_source, "html.parser")


def check_redirection(src_url):
    """Update the graph w.r.t. redirections and return True if the CALLBACKS
    ought to be called on the current node

    :param src_url: the current url (what we told the webdriver to get())
    :type src_url: str

    .. note:: This function assumes it is called after the call to DRIVER.get,\
    but before calls to add_url.
    """
    dst_url = xs.DRIVER.current_url

    # No redirection
    if src_url == dst_url:
        return True

    xs.LOGGER.info("Redirection from {} to {}\n".format(src_url, dst_url))
    # The page we ended up on is not the one we wanted to load => Redirection
    # from src_url to dst_url
    dst_url_was_known = dst_url in xs.G
    src_url_was_known = src_url in xs.G

    assert (
        src_url_was_known
    ), """Why the fuck are we visiting a page that is not in the graph ?"""
    assert (
        xs.G.nodes[src_url]["seen"] == Seen.YET_TO_BE_SEEN
    ), "Why are we visiting a node that was not to be seen ?"

    if not dst_url_was_known:
        # Create the destination node after redirection
        xs.LOGGER.debug("Redirection destination {} was not known.".format(dst_url))
        xs.G.add_node(dst_url, **xs.G.nodes[src_url])

    elif dst_url_was_known:
        # Update the destination node after redirection
        xs.LOGGER.debug("Redirection destination {} was known.".format(dst_url))
        xs.G.nodes[dst_url]["root"] = (
            xs.G.nodes[dst_url]["root"] or xs.G.nodes[src_url]["root"]
        )
        xs.G.nodes[dst_url]["priority"] = max(
            xs.G.nodes[url]["priority"] for url in [dst_url, src_url]
        )

    xs.G.nodes[src_url]["seen"] = Seen.SEEN
    # A redirection node is characterized by a type argument with "redirection value"
    xs.G.nodes[src_url]["type"] = "redirection"
    # Between the redirection node (src) and the destination node (dst), the edge weight is null
    xs.G.add_edge(src_url, dst_url, weight=0)

    # do not call the callbacks on dst if dst['seen'] != YET_TO_BE_SEEN
    xs.LOGGER.debug(
        "Redirection destination 'seen status' is {}".format(
            xs.G.nodes[dst_url]["seen"]
        )
    )

    return xs.G.nodes[dst_url]["seen"] == Seen.YET_TO_BE_SEEN
