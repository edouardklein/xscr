"""
xscraper
-----------

Introduction
~~~~~~~~~~~~

xscraper is a python language software for web crawling and scraping.

xscraper can chain up to three proxies:

- TOR or a VPN for anonymization

- mitmproxy (https://github.com/mitmproxy/mitmproxy), an interactive SSL-capable intercepting HTTP proxy

- polipo (https://github.com/jech/polipo), a caching HTTP proxy.

The dump of all HTTP transactions that mitmproxy creates ensures that anything
that was shown on screen is stored somewhere on the disk.

The website currently being scraped is represented internally as a graph
(:py:data:`G`) where each page is a node and each link between two pages is
an edge.

At its core, xscraper is a loop that calls all :py:data:`CALLBACKS`
functions, on the nodes of a graph :py:data:`G`.

Some :py:mod:`default_callbacks` are provided.

A specific callback, :py:data:`CALLBACK_WHEN_EMPTY`, is called when all the
nodes in the graph :py:data:`G` have been visited.

Roughly, callbacks fall in two categories:

 - crawling callbacks, that will find relevant links on a page and add the \
target urls to the graph
 - scraping callbacks, that will find relevant information on a page and \
store it within the nodes (nodes are dictionary and thus support arbitrary \
key-value mappings)


Global Variables
~~~~~~~~~~~~~~~~

The main loop of xscraper uses global variables to store its state.
Those variables are meant to be changed by the callbacks.
"""

__version__ = "1.0.5"

LOGGER = None
""" Logging Object"""

CAMPAIGN = ""
"""The name of this scraping campaign."""

TIMESTAMP = ""
"""The date and time when xscraper was launched, as a string"""
# FIXME: Use UTC

PROGRAMS = []
"""Daemons that have been launched and need to be stopped before quitting.
See :py:mod:`daemons`."""

LOCK = None
"""Lock that protect modifications to :py:data:`CALLBACKS` and
:py:data:`CALLBACKS_INDEX`"""

DRIVER = None
"""Our only instance of the selenium web driver"""

CURRENT_SOUP = None
"""The BeautifulSoup object for the page currently being analyzed"""

CALLBACKS = []
"""Functions that will be called when a new page is loaded by the web driver.

Callback functions can modify this list at will."""

CALLBACKS_INDEX = 0
"""Index of the callback currently being executed. This variable can be \
changed by callback functions.
"""

G = None
"""The website being scraped is represented internally as a graph.

**Node :** A node store all relevant data about URLs.
A node is expected to have at least four fields (see :py:func:`graph.add_url`
for the preferred method of adding nodes and edges to the graph):
- seen (:py:class:`graph.Seen`),
- priority,
- root
- weight

**Edge :** An edge exists between node A and node B if the page at the URL of
node A contains a link to the url of node B.
"""

CALLBACK_WHEN_EMPTY = None
"""Called when there is no node left whose seen attribute is Seen.TO_BE_SEEN.

If the return value is True, then xscraper exits."""

FORM_MEMORY = None
""" Store all form data"""
