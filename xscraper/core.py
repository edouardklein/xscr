"""Core
-------------------------
xscraper's startup procedure relies on the following modules:
- :py:mod:`cli` handles the parsing of command line arguments,
populating most global variables in the process.
- :py:mod:`signals` is where all signal handlers are defined
- :py:mod:`daemons` is maybe going to disappear
- :py:mod:`gui` should not exist, this functionality should be
integrated into networkx
- :py:mod:`scram` is tasked to shutdown xscraper

.. todo::

    Faire disparaitre gui et daemons.

The entry point is the :py:func:`main` function.
"""

import docopt
import logging
import pickle
import time
import os
import xscraper as xs
import xscraper.cli as cli
import xscraper.selenium as selenium
import xscraper.scram as scram
from xscraper import __version__
from xscraper.daemons import start_proxies, check_and_handle_504
from xscraper.signals import install_signals
from xscraper.graph import Seen, check_redirection, update_current
from xscraper.default_callbacks import console_once, console
from datetime import datetime
from importlib.util import spec_from_loader, module_from_spec
from importlib.machinery import SourceFileLoader
from selenium.common.exceptions import WebDriverException, TimeoutException
from urllib.parse import urlparse


def start_logging(level=None):
    """Start logging both to the console and to a logfile.

    The logfile stores every message, but the console only displays
    messages up from the user-specified level (warning by default).
    """
    xs.LOGGER = logging.getLogger("logging")
    logging_lvl = {
        "info": logging.INFO,
        "debug": logging.DEBUG,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "critical": logging.CRITICAL,
    }

    if level is None:
        level = "warning"
    if level not in logging_lvl:
        raise RuntimeError("Unknown logging level: {}".format(level))

    xs.LOGGER.setLevel(logging.DEBUG)

    # Console display
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging_lvl[level])

    # Logging file
    format = logging.Formatter("%(asctime)s, %(levelname)s, %(message)s")
    file_handler = logging.FileHandler("{}-{}.log".format(xs.TIMESTAMP, xs.CAMPAIGN))
    file_handler.setFormatter(format)

    xs.LOGGER.addHandler(stream_handler)
    xs.LOGGER.addHandler(file_handler)


def load_module(arguments):
    """Exec some user-provided code."""
    if arguments["--module"]:
        xs.LOGGER.info("Launching module {}".format(arguments["--module"]))
        spec = spec_from_loader(
            arguments["--module"],
            SourceFileLoader(arguments["--module"], "./" + arguments["--module"]),
        )
        module = module_from_spec(spec)
        spec.loader.exec_module(module)
        # We replace the name of the plugin file by the module reference created during the loading of the plugin file
        # It is necessary to be able to use the imported script everywhere (context conflict else)
        arguments["--module"] = module
    return arguments


def load_cookie(arguments=None):
    xs.LOGGER.info("Loading cookies")
    _cookies = pickle.load(open(arguments["--load-cookies"], "rb"))
    cookies = [
        cookie
        for cookie in _cookies
        if cookie["domain"] == urlparse(xs.DRIVER.current_url).netloc
    ]
    if not cookies:
        raise ValueError(
            f"No cookie found for the {urlparse(xs.DRIVER.current_url).netloc} domain"
        )

    xs.DRIVER.delete_all_cookies()
    for cookie in cookies:
        xs.LOGGER.warning(cookie)
        xs.DRIVER.add_cookie(cookie)
        xs.LOGGER.warning("Cookies loaded from previous session")


def recovery_connection(current_url, tor_activation=False):
    """
    Refresh the page (and) Restart Tor when a problem is detected with the connection
    :param current_url: URL of the current page
    :param tor_activation: If True, Tor connection will be refreshed else not
    """
    error_restart = 0
    while True:
        if tor_activation:
            xs.LOGGER.warning("Recovery of the connection...")
            tor_daemux = [p for p in xs.PROGRAMS if p.cmd.startswith("tor")][0]
            xs.LOGGER.warning("Restart Tor...")
            tor_daemux.restart()
            xs.LOGGER.info("OK")
        try:
            xs.DRIVER.get(current_url)
            update_current()
            break
        except (WebDriverException, TimeoutException):
            error_restart += 1
            # Empirical observation: Waiting few seconds can be efficient
            xs.LOGGER.info("Waiting before restarting...")
            time.sleep(60)
            if error_restart > 3:
                break

    if error_restart > 3:
        console(
            "The requested page: {} did not load. Please correct"
            " any error and hit ctrl-D to try again.".format(current_url)
        )


def check_tor_init():
    """
    Check if the Tor connection is activated and operational
    """
    check_message = "Congratulations. This browser is configured to use Tor."
    while True:
        xs.DRIVER.get("https://check.torproject.org/")
        xs.core.update_current()

        # If it does not, relaunch the tor process
        if check_message not in str(xs.CURRENT_SOUP.find_all("title")[0]):
            xs.LOGGER.warning(f"There is a problem about Tor initialization")
            recovery_connection("https://check.torproject.org/", tor_activation=True)
        else:
            break


def loop(arguments=None):
    """xscraper core loop function"""
    # FIXME: Tirer ces infos du graphe, et cesser de maintenir ces variables
    # Scraper's initialization
    # scraped: number of pages already scraped
    scraped = 0
    ref_time = time.time()

    if arguments["--tor"]:
        check_tor_init()

    # Main loop
    while True:
        current_time = time.time()
        # Automatic dump of the graph each hour
        if current_time - ref_time >= 3600:
            xs.scram.dump_graph()
            ref_time = time.time()
            xs.LOGGER.info("Export of the graph - Success")

        # Search the next URL to scrap. If there is not any url to scrap, False is returned
        current_url = next(
            (
                url
                for url in xs.G.nodes
                if xs.G.nodes[url]["seen"] == Seen.YET_TO_BE_SEEN
            ),
            False,
        )
        # queue: number of pages yet to scrap
        queue = len(
            [
                url
                for url in xs.G.nodes
                if xs.G.nodes[url]["seen"] == Seen.YET_TO_BE_SEEN
            ]
        )

        if current_url is False:
            ctrl_ending = xs.CALLBACK_WHEN_EMPTY()
            # if True, quit the loop function to quit Gs
            if ctrl_ending is True:
                return 0
            else:
                continue

        # Opening page in the webdriver
        xs.LOGGER.info(
            "Working on {} : {} scraped/{} in queue".format(current_url, scraped, queue)
        )
        try:
            # to fight against 429 Too many request error from the distant server
            if arguments["--slow_nav"]:
                xs.LOGGER.debug("Waiting...")
                time.sleep(int(arguments["--slow_nav"]))

            xs.DRIVER.get(current_url)
            # We can't add cookies before being on a page with the same domain than the cookie
            if scraped == 0 and arguments["--load-cookies"]:
                load_cookie(arguments)
                xs.DRIVER.get(current_url)

            update_current()
            nb_restart = check_and_handle_504(0, current_url)
            while nb_restart != 0:
                nb_restart = check_and_handle_504(nb_restart, current_url)
            if not check_redirection(current_url):
                continue

            update_current()

        except TimeoutException:
            # The page takes too much time to load
            # Probably a problem from Tor proxy so we will relaunch it (if Tor is used...)
            if arguments["--tor"]:
                recovery_connection(current_url)
            else:
                console(
                    "The requested page: {} did not load. Please correct"
                    " any error and hit ctrl-D to try again.".format(current_url)
                )

        except WebDriverException as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            xs.LOGGER.warning(message)
            console(
                "The requested page: {} did not load. Please correct"
                " any error and hit ctrl-D to try again.".format(current_url)
            )

        # We can't use a simple for loop,
        # because functions in the CALLBACKS list are supposed to
        # be able to modify CALLBACKS while iterating,
        # and people that modify a list when iterating over it
        # are evil people.
        with xs.LOCK:
            xs.CALLBACKS_INDEX = 0
        while xs.CALLBACKS_INDEX < len(xs.CALLBACKS):
            try:
                xs.CALLBACKS[xs.CALLBACKS_INDEX]()
            except Exception:
                xs.LOGGER.exception("A callback raised an exception.")
                with xs.LOCK:

                    def manage_error():
                        """Self removing callback"""
                        console_once(
                            header="Giving control back to the user"
                            " to manage the error. Hit Ctrl-D when"
                            " done."
                        )

                    xs.CALLBACKS.insert(xs.CALLBACKS_INDEX + 1, manage_error)
            with xs.LOCK:
                xs.CALLBACKS_INDEX += 1

        xs.G.nodes[xs.DRIVER.current_url]["seen"] = xs.graph.Seen.SEEN

        # Incrementing for information display
        scraped += 1


def main():
    """xscraper's entry point.

    - First, we parse the commandline arguments using docopt.

    - Then, we start logging.

    - We populate most global variables from the command line arguments

    - We start the proxies

    - We start the http server

    - We start the websocket server

    - We start selenium

    And finally we hand control over to the main loop: :py:func:`loop`.

    When the main loop exits or raises an exception, the shutdown procedure
    begins."""

    arguments = docopt.docopt(cli.__doc__, version="xscraper v" + __version__)
    xs.TIMESTAMP = datetime.now().strftime("%Y%m%d-%H:%M:%S")
    xs.CAMPAIGN = cli.get_name(arguments)
    start_logging(arguments["--logging"])

    xs.LOGGER.debug("Arguments:\n{}\n".format(arguments))
    try:
        arguments = cli.init_globals(arguments)
        arguments["tag"] = str(os.getpid()) + "xscraper"
        start_proxies(arguments)
        install_signals(arguments)
        selenium.start(arguments)
        arguments = load_module(arguments)
        loop(arguments)

    except Exception:
        xs.LOGGER.exception(
            "Uncaught exception in the main loop, " "emergency shutdown."
        )
        scram.dump_graph(emergency=True)
        scram.dump_form_value()
        scram.shutdown(emergency=True, arg=arguments)
    else:
        print("SUCCESS")
        scram.dump_graph()
        scram.dump_form_value()
        scram.shutdown(arg=arguments)
