"""PhpBB
-----------

Scraping module for at least one version of PhpBB.

This module do not scrap the pages. Be sure to launch xscraper with the
`--mitmproxy` option !
"""
import xscraper as xs
from xscraper.graph import add_url, Seen, update_current
from xscraper.default_callbacks import console


def back_to_topic_collection():
    """Switch back to the topic collection stage if there is an unvisited
    topic list in the graph"""
    topic_list = [n for n in xs.G if xs.G.nodes[n]["type"] == "Unvisited topic list"]
    if len(topic_list) > 0:
        for n in topic_list:
            xs.LOGGER.info("Marking topic list {} as to be seen.".format(n))
            xs.G.nodes[n]["seen"] = Seen.YET_TO_BE_SEEN
    else:
        xs.LOGGER.info("No unvisited topic list, quitting.")
        return True  # Quit Xscraper

    xs.LOGGER.debug("Setting callbacks for the next stage")
    xs.CALLBACKS = [topic_collection]
    xs.CALLBACK_WHEN_EMPTY = topic_collection_to_topic_visit


def topic_visit():
    """Visit the topic, including the next pages, and change the type of the
    first page from 'Unvisited topic' to 'Visited topic'

    Note that the next pages are not added to the graph, they a re just
    visited."""
    topic_first_page = xs.DRIVER.current_url
    xs.LOGGER.info("Visiting {}".format(topic_first_page))
    while True:
        try:
            next_page = [
                "" + a.attrs["href"][2:] for a in xs.CURRENT_SOUP.select("a.right-box")
            ][0]
            xs.DRIVER.get(next_page)
            update_current()
            xs.LOGGER.info("Next page, same topic : {}".format(xs.DRIVER.current_url))
        except IndexError:
            xs.LOGGER.info("This was the last page of this topic")
            break
    xs.LOGGER.info("Changing page type to 'visited topic'.")
    xs.G.nodes[topic_first_page]["type"] = "Visited topic"


def topic_collection_to_topic_visit():
    """Switch to the 'visit the topics' stage"""
    xs.LOGGER.info("Marking all unvisited topics as to be visited")
    for _ in [n for n in xs.G if xs.G.nodes[n]["type"] == "Unvisited topic"]:
        xs.CALLBACK_WHEN_EMPTY = back_to_topic_collection
        xs.CALLBACKS = [topic_visit]


def topic_collection():
    """Topic collection stage"""
    xs.LOGGER.info("Adding topics to the graph:")
    for a in ["" + a.attrs["href"][2:] for a in xs.CURRENT_SOUP.select("a.topictitle")]:
        add_url(
            a, xs.DRIVER.current_url, seen=Seen.NOT_TO_BE_SEEN, type="Unvisited topic"
        )
        xs.LOGGER.info("\t{}".format(a))

    try:
        next_topic_list = [
            "" + a.attrs["href"][2:] for a in xs.CURRENT_SOUP.select("a.right-box")
        ][0]
        xs.LOGGER.info("Adding next topic list to the graph {}".format(next_topic_list))
        # To do a breadth-first scrap, switch the seen flag
        # to YET_TO_BE_SEEN here
        add_url(
            next_topic_list,
            xs.DRIVER.current_url,
            seen=Seen.NOT_TO_BE_SEEN,
            type="Unvisited topic list",
        )
    except IndexError:
        xs.LOGGER.info("This was the last page of the topic list.")

    xs.G.nodes[xs.DRIVER.current_url]["type"] = "Visited topic list"
    xs.CALLBACK_WHEN_EMPTY = topic_collection_to_topic_visit


def interactive_to_topic_collection():
    """Go from the interactive stage to the topic collection stage"""
    xs.LOGGER.debug("Adding a 'type' field to all the nodes in the graph")
    for n in xs.G:
        xs.G.nodes[n]["type"] = "Node that was there from the beginning"

    xs.LOGGER.info("Adding {} to the graph.".format(xs.DRIVER.current_url))
    add_url(
        xs.DRIVER.current_url,
        [n for n in xs.G if xs.G.nodes[n]["root"]][0],
        type="Unvisited topic list",
    )

    xs.LOGGER.debug("Setting callbacks for the next stage")
    xs.CALLBACKS = [topic_collection]
    xs.CALLBACK_WHEN_EMPTY = topic_collection_to_topic_visit


xs.CALLBACKS = [
    lambda: console(
        "Please log into the forum, "
        + "navigate to the start page, "
        + " press CTRL-D when done."
    )
]
xs.CALLBACK_WHEN_EMPTY = interactive_to_topic_collection
