"""Daemons
-----------

Manage the background processes xscraper relies on.
"""

import xscraper as xs
import daemux
from daemux import Daemon

# FIXME: remove this import after libtmux's bug gets fixed (see below)
import libtmux

# FIXME: This one too
import time
import os
from xscraper.default_callbacks import console
from xscraper.graph import update_current
from datetime import datetime


# FIXME: update daemux package si tgat the timeout argument can be passed to daemux.start
def start(cmd, timeout=20, **kwargs):
    """Modify the start function of daemux package to be able to modify the timeout. 10 sec is too low !
    Delete it when the daemux package will be updated.
    """
    # There is a bug in libtmux
    # FIXME: open an issue and make the upstream bug get fixed
    # When a session does not exist, calling server.new_session will fail with:
    #  File "/gnu/store/zljy9nmjp4kgw4by9q86pgpqx8h028ml-profile/lib/python3.8/site-packages/libtmux/session.py", line 57, in __init__
    # raise ValueError('Session requires a `session_id`')
    # ValueError: Session requires a `session_id`
    # But this error disappears when the session exists.
    # To avoid this bug we create the session by hand and catch the ValueError
    # FIXME: Make sure to remove this workaround once the libtmux bug is fixed.
    # Also, it may be because of LANG, see:
    # https://github.com/tmux-python/libtmux/issues/265

    assert "session" in kwargs
    s = libtmux.Server()
    nb_tries = 0
    while not s.has_session(kwargs["session"]):
        try:
            s.new_session(kwargs["session"])
        except ValueError:
            time.sleep(0.5)
            nb_tries += 1
            print(f"Creating tmux session, {kwargs['session']}, try #{nb_tries}")
    # End of workaround
    answer = Daemon(cmd, **kwargs)
    answer.start(timeout=timeout)
    return answer


def launch_and_check(cmd, expected_output=None, timeout=20, tag=""):
    """Launch cmd and wait for the expected output to appear"""
    cmd_str = " ".join(cmd)
    cmd_daemux = start(
        cmd_str, session=tag, window=xs.CAMPAIGN, pane=len(xs.PROGRAMS), timeout=timeout
    )  # We reuse panes
    xs.PROGRAMS.append(cmd_daemux)
    if expected_output is not None:
        cmd_daemux.wait_for_output(expected_output, timeout=timeout)


def check_and_handle_504(nb, url=None):
    """Restart a badly behaving tor process."""
    if nb > 3:
        # If Tor proxy error, xscraper gives the lead to the user for fixing the problem
        # Tor proxy error is not a critical intern error of xscraper. It's not necessary to kill the xscraper
        # instance because the problem can be resolved by the user manually and without modifications of
        # xscraper's core.
        console(
            header="We relaunched the tor process 3 times and "
            "still get a 504. Please resolve the problem and press Cntrl-D"
        )
        xs.DRIVER.get(url)
        update_current()
    error_msg = "Proxy error: 504 Connect to"
    try:
        if error_msg in str(xs.CURRENT_SOUP.find_all("title")[0]):
            # Reload, it may work.
            xs.DRIVER.refresh()
            xs.graph.update_current()
        # If it does not, relaunch the tor process
        if error_msg in str(xs.CURRENT_SOUP.find_all("title")[0]):
            xs.LOGGER.error("We get a 504, restarting the tor daemon...")
            tor_daemux = [p for p in xs.PROGRAMS if p.cmd.startswith("tor")][0]
            xs.LOGGER.info("Restart Tor...")
            tor_daemux.restart()
            # tor_daemux.stop()
            # tor_daemux.start()
            # Why the % symbol (Bootstrapped 100%: Done) crashes the command ?
            # tor_daemux.wait_for_output('Bootstrapped 100', timeout=20)
            xs.LOGGER.info("OK")
            return nb + 1
        else:
            return 0
    # if no <title>, it's not a 504 error page so continue
    except IndexError:
        return 0


def start_proxies(arguments):
    """Start and chain all the proxies we need.

    .. figure:: ../docs/proxies.png
       :align: center
       :alt: All possible proxy configurations

       All possible proxy configurations

    """
    xs.LOGGER.debug("In start proxies...")
    tor_cmd = ["tor"] if arguments["--tor"] else None
    if tor_cmd is not None:
        # By default, Tor listens on 9050
        tag = datetime.now().strftime(f"%m%d%Y%Ih%Mm%S")
        tor_port = arguments["--port_tor"] if arguments["--port_tor"] else 9050
        tor_cmd.append("--SocksPort")
        tor_cmd.append(str(tor_port))
        tor_cmd.append("--DataDirectory")
        tor_cmd.append(str("/tmp/tor_" + tag))

    polipo_cmd = ["polipo"]
    # By default, Polipo listens on 8123
    polipo_port = arguments["--port_polipo"] if arguments["--port_polipo"] else "8123"

    mitm_cmd = ["mitmdump"]
    # By default, Mintmproxy listens on 8080
    mitm_port = (
        arguments["--port_mitmproxy"] if arguments["--port_mitmproxy"] else "8080"
    )

    if tor_cmd is not None:
        assert polipo_cmd, (
            "We can not chain directly to tor, we must go" "through polipo first."
        )
        polipo_cmd.append("socksParentProxy=localhost:{}".format(tor_port))

    polipo_cmd.append("proxyPort={}".format(polipo_port))
    mitm_cmd.append("--mode")
    mitm_cmd.append("upstream:http://127.0.0.1:{}".format(polipo_port))
    mitm_cmd.extend(
        [
            "-p",
            str(mitm_port),
            "-w",
            "+{}-{}.replay".format(xs.TIMESTAMP, xs.CAMPAIGN),
            "--anticache",
        ]
    )

    if arguments["--mitmproxy-no-security"]:
        mitm_cmd.append("--ssl-insecure")

    if arguments["--mitmproxy_script"]:
        mitm_cmd.extend(
            ["-s"]
            + arguments["--mitmproxy_script"].split(" ")
            + ["--set", f"date='{str(datetime.now().strftime('%Y%m%d'))}'"]
        )

    xs.LOGGER.debug("tor_cmd: {}".format(tor_cmd))
    xs.LOGGER.debug("polipo_cmd: {}".format(polipo_cmd))
    xs.LOGGER.debug("mitm_cmd: {}".format(mitm_cmd))

    print("Program PID (for expert only): " + str(os.getpid()))
    if tor_cmd:
        print("Launching TOR")
        launch_and_check(
            tor_cmd,
            expected_output="Bootstrapped 100%",
            tag=arguments["tag"],
            timeout=30,
        )
        print("Success")

    if polipo_cmd:
        print("Launching Polipo")
        launch_and_check(polipo_cmd, tag=arguments["tag"])
        print("Success")

    if mitm_cmd:
        print("Launching Mitmproxy")
        launch_and_check(
            mitm_cmd, expected_output="Proxy server listening", tag=arguments["tag"]
        )
        print("Success")

    # If we don't put a nice layout, it may be impossible to add new panes,
    # because tmux complains the panes are too small
    # FIXME: Daemux now has a layout=... option
    try:
        xs.PROGRAMS[0].window.select_layout("tiled")
    except IndexError:
        xs.LOGGER.warning("There is no daemon. Make sure you use a VPN.")
