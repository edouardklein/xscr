"""
Command-line interface
------------------------
Usage:
    xscraper <url>... (--tor | --VPN) [(--form=<form> [--auto_form][--import_form=<import_form>])]
    [--port_websocket=<port_websocket>][--port_server=<port_server>][--domain=<domain>] [--depth=<depth>][--name=<name>]
    [--scroll=<scroll_duration>][--inf_scroll][--only=<white_regex>][--except=<black_regex>][--console][--except_first]
    [--module=<module>][--special_stop=<stop_regex>][--screenshot][--save-cookies][--except_first]
    [--load-cookies=<cookies-infile>][--restore=<restore>][--logging=<logging>][--except_first][--auto_add_page][--no_domain_filter]
    [--browser-plugin=<browser-plugin>][--mitmproxy-no-security][--random-user-agent][--slow_nav=<slow_nav>][--aap_page_analysis=<aap_page_analysis>]
    [--browser_parameters=<browser_parameters>][--port_mitmproxy=<port_mitmproxy>][--port_tor=<port_tor>][--port_polipo=<port_polipo>]
    [--mitmproxy_script=<mitmproxy_script>] [--dl-file-repertory=<dl-file-repertory>] [--dl-pdf]
    [(--auto_log_page=<auto_log_page> [--max_page=<max_page>] [--max_time=<max_time>])]

Options:
    --domain=<domain>                          Specify the domain xscraper will stay on.
    --name=<name>                              Specify a name to this scraping.
    --depth=<depth>                            Specify the depth (number of links from start page) of the scraping
    --scroll=<scroll_duration>                 Scroll down on every page, for a duration of scroll_duration (seconds)
    --inf_scroll                               Scroll down on every page until the page ending
    --screenshot                               Take screenshots of every pages loaded (after scrolling if set)
    --only=<white_regex>                       Just do links catched by regex.
    --except=<black_regex>                     Skip links catched by regex.
    --except_first                             If a whitelist tag and a blacklist tag are raise, the blacklist tag will dominate
                                               with this option. By default, whitelist tag dominate.
    --special_stop=<stop_regex>                Stop if regex matches page content.
    --restore=<restore>                        Restore latest scraping, if something went wrong...
    --port_websocket=<port_websocket>          Port used by websocket, necessary for display graph in Web browser
    --port_server=<port_server>                Port used by Flask to create server
    --save-cookies                             Save cookies to file
    --load-cookies=<cookies-infile>            Load cookies from file
    --console                                  Open a console at each
    --form=<form>                              Enable Form interaction (manual or interactive)
    --auto_form                                Autofill form according to an extern JSON file
    --import_form=<import_form>                Import filled form data to automatize the scraping
    --auto_add_page                            Interface to detect page changing according to browser interactions by the user
    --module=<module>                          Use plugin of xscraper
    --tor                                      Use proxy tor configuration ( with polipo )
    --VPN                                      Use VPN Proxy configuration
    --logging=<logging>                        Logging level
    --no_domain_filter                         Don't blacklist all page with a different domain of the URL given to xscraper
    --browser-plugin=<browser-plugin>          Install plugin for a custom browser
    --browser_parameters=<browser_parameters>  Custom the browser with specific parameters (see https://peter.sh/experiments/chromium-command-line-switches/)
    --mitmproxy-no-security                    Ignore SSL certificate (Expert only - useful for specific scraping)
    --mitmproxy_script=<mitmproxy_script>      Import a custom plugin for mitmproxy
    --random-user-agent                        Use a custom agent user (Anti-bot countermeasure)
    --slow_nav=<slow_nav>                      Slow the velocity of the scrap, specially to limit the 429 Too many requests error
    --aap_page_analysis=<aap_page_analysis>    Use a specific function (imported by --module)on each visited page in the auto_add_page environment
    --port_mitmproxy=<port_mitmproxy>          Choose a specific port for mitmproxy
    --port_tor=<port_tor>                      Choose a specific port for tor
    --port_polipo=<port_polipo>                Choose a specific port for polipo
    --dl-file-repertory=<dl-file-repertory>    Choose a specific repertory for all downloads from the browser
    --dl-pdf                                   Desactivate the pdf viewer and download directly the pdf file - Useful if you want download a pdf from a website
    --auto_log_page=<auto_log_page>            Logout to relogin automatically - Useful to bypass some countermeasures
    --max_page=<max_page>                      Logout after max_page visited  [default: 2000]
    --max_time=<max_time>                      Logout after a session time higher than max_time (second) [default: 3600]


"""

import os
import re
import networkx
import json
import xscraper as xs
from xscraper.default_callbacks import (
    screenshot,
    get_default_pause,
    infinite_scroll,
    get_scroll_down,
    get_default_crawler,
    console,
    get_form_pause,
    auto_log_page,
)
from xscraper.graph import add_url
from threading import RLock
from pprint import pformat
from urllib.parse import urlparse
from xscraper.default_callbacks import auto_add_page


def init_globals(arguments):
    """Initialise most global variables from the command line arguments.

    :param arguments: Command line arguments as parsed from docopt
    :type arguments: dict
    """

    xs.LOCK = RLock()

    if arguments["--restore"]:
        xs.LOGGER.warning("Restoring from {}".format(arguments["--restore"]))
        xs.G = networkx.read_gpickle(arguments["--restore"])
    else:
        assert (
            arguments["<url>"] is not None
        ), "Please provide a URL to start from, or a graph to restore from."
        xs.G = networkx.DiGraph()
        for url in arguments["<url>"]:
            add_url(url, root=True)

    # Some conditions trigger a console
    if arguments["--console"] or arguments["--special_stop"]:
        xs.CALLBACKS.append(
            get_default_pause(arguments["--special_stop"], arguments["--console"])
        )

    if arguments["--console"]:
        xs.CALLBACK_WHEN_EMPTY = lambda: console("No more urls to be seen.")
    else:
        xs.CALLBACK_WHEN_EMPTY = lambda: True

    if arguments["--scroll"]:
        xs.CALLBACKS.append(get_scroll_down(int(arguments["--scroll"])))

    if arguments["--inf_scroll"]:
        xs.CALLBACKS.append(lambda: infinite_scroll(log=True))

    if arguments["--screenshot"]:
        if not os.path.exists(xs.CAMPAIGN + "_screenshots"):
            os.mkdir(xs.CAMPAIGN + "_screenshots")
        xs.CALLBACKS.append(lambda: screenshot(xs.CAMPAIGN))

    if arguments["--auto_add_page"]:
        xs.CALLBACKS.append(lambda: auto_add_page(arguments))

    if arguments["--form"]:
        xs.CALLBACKS.append(
            get_form_pause(
                arguments["--form"],
                arguments["--import_form"],
                arguments["--auto_form"],
                arguments["--screenshot"],
            )
        )

    if arguments["--auto_log_page"]:
        if not arguments["--auto_log_page"]:
            raise ValueError("No imported file - Please, import a valid file !")
        else:
            with open(arguments["--auto_log_page"], "r") as f:
                xs.FORM_MEMORY = json.load(f)

        log_page_object = auto_log_page(
            arguments["--max_page"], arguments["--max_time"]
        )
        xs.CALLBACKS.append(lambda: log_page_object.execute())

    if arguments["--depth"]:
        depth = int(arguments["--depth"])
    else:
        depth = None

    black_regex = re.compile(arguments["--except"]) if arguments["--except"] else None
    white_regex = re.compile(arguments["--only"]) if arguments["--only"] else None
    xs.CALLBACKS.append(
        get_default_crawler(
            depth,
            black_regex,
            white_regex,
            arguments["--except_first"],
            xs.CAMPAIGN if not arguments["--no_domain_filter"] else None,
        )
    )

    # When testing, bypass Tor and VPN configuration
    if ("localhost" or "127.0.0.1") in arguments["<url>"][0]:
        arguments["--VPN"] = None
        arguments["--tor"] = None

    xs.LOGGER.debug(
        "init_globals() is done:\n{}".format(
            pformat(
                {a: xs.__getattribute__(a) for a in dir(xs) if not a.startswith("_")}
            )
        )
    )
    return arguments


def display_information():
    """Display of useful signals
    SIGTERM Exit all process cleanly
    SIGINT Exit cleanly, let Tor run
    SIGUSR1 Drop to the console
    You can use SIGTERM in ipython console
    """
    xs.LOGGER.warning("Command to execute signal")
    xs.LOGGER.warning("kill -SIGUSR1 " + str(os.getpid()))
    xs.LOGGER.warning("kill -SIGTERM " + str(os.getpid()))


def get_name(arguments):
    """Return the campaign name as specified in, or deduced from,
    the arguments."""
    if arguments["--name"] is not None:
        return arguments["--name"]
    if arguments["--domain"] is not None:
        return arguments["--domain"]
    assert (
        "://" in arguments["<url>"][0]
    ), "You should specify a protocol (e.g. http://...) in the URL"
    return urlparse(arguments["<url>"][0]).netloc.replace("www", "")
