"""Shutdown
-------------"""

import xscraper as xs
import xscraper.selenium as selenium
import os
import sys
import json
import libtmux
from signal import SIGINT
import networkx as nx


def dump_graph(emergency=False):
    """Save G in a file."""
    fname = "{}-{}{}.pickle".format(xs.TIMESTAMP,
                                  xs.CAMPAIGN,
                                  "_errdump" if emergency else "")
    xs.LOGGER.info("Saving the graph in {}...".format(fname))
    nx.write_gpickle(xs.G, fname)
    xs.LOGGER.info("OK")


def dump_form_value():
    """Save form values in a file"""
    fname = "{}-{}{}.json".format(xs.TIMESTAMP,
                                  xs.CAMPAIGN,
                                  "_form_data")
    xs.LOGGER.info("Saving form data in {}...".format(fname))
    with open(fname, "w", encoding="utf-8") as file:
        json.dump(xs.FORM_MEMORY, file, indent=4)
    xs.LOGGER.info("OK")


def shutdown(emergency=False, arg=None):
    """Shutdown nicely."""
    xs.LOGGER.info('Shutdown xscraper...')
    selenium.stop(arg['--save-cookies'])
    for p in xs.PROGRAMS:  # stops daemons mitmdump tor polipo
        xs.LOGGER.info('Stopping {}...'.format(p.cmd.split()[0]))
        p.stop()
        xs.LOGGER.info('OK')
    xs.LOGGER.info('Stopping the websocket server...')
    # Websocket is only available as an asyncio coroutine
    # I don't want to turn xscraper into a bunch of asyncio coroutines
    # therefore I launched the websocket server in a separate process
    # But it does not want to quit.
    # So we just kill it there.
    #try:
        #os.kill(xs.PROCESS.pid, SIGINT)
        #xs.LOGGER.info('OK')
    #except AttributeError as e:
        # Unkillable most likely because it was not launched yet...
        #xs.LOGGER.warning("Failed to kill PROCESS.pid : " + str(e))
    xs.LOGGER.info(f'Stopping xscraper interface')
    libtmux.Server().find_where({"session_name": arg["tag"]}).kill_window(arg["tag"])
    xs.LOGGER.info('Goodbye !')
    sys.exit(0 if not emergency else 1)
