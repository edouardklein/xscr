from selenium import webdriver
import xscraper as xs
import pickle
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem
import os


def start(arguments):
    """Start the Selenium web driver."""
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-gpu")

    if arguments["--dl-pdf"]:
        prefs = {
            "plugins.always_open_pdf_externally": True,
            "download.default_directory": "/tmp",
        }

        if arguments["--dl-file-repertory"]:
            prefs["download.default_directory"] = arguments["--dl-file-repertory"]
            if not os.path.isdir(arguments["--dl-file-repertory"]):
                os.mkdir(arguments["--dl-file-repertory"])

        chrome_options.add_experimental_option("prefs", prefs)

    if arguments["--browser_parameters"]:
        for param in arguments["--browser_parameters"].split(" "):
            chrome_options.add_argument(param)

    # By default, Mitmproxy listens on 8080
    mitm_port = (
        arguments["--port_mitmproxy"] if arguments["--port_mitmproxy"] else "8080"
    )
    chrome_options.add_argument("--proxy-server=127.0.0.1:{}".format(mitm_port))
    # Chromium will bypass the proxy when the connection is local, we don't want that
    chrome_options.add_argument("--proxy-bypass-list=<-loopback>")

    if arguments["--browser-plugin"]:
        for plugin in arguments["--browser-plugin"].split(" "):
            chrome_options.add_extension(plugin)

    if arguments["--random-user-agent"]:
        # Giving a fake user agent is a shitty thing to do,
        # but it may prove necessary against bad actors.
        # Beware, it can be detected.
        software_names = [SoftwareName.CHROME.value]
        operating_systems = [OperatingSystem.LINUX.value]
        user_agent_rotator = UserAgent(
            software_names=software_names, operating_systems=operating_systems, limit=1
        )
        user_agent = user_agent_rotator.get_random_user_agent()
        chrome_options.add_argument(f"user-agent={user_agent}")
        xs.LOGGER.info(f"Fake user-agent: {user_agent}")

    xs.LOGGER.info(
        "\nLaunching webdriver with options {}...".format(chrome_options.arguments)
    )
    xs.DRIVER = webdriver.Chrome(options=chrome_options)
    xs.DRIVER.set_window_position(0, 0)
    xs.LOGGER.info("OK")


def save_cookies():
    """Dump current cookies into a file"""

    fname = "{}-{}{}.pickle".format(xs.TIMESTAMP, xs.CAMPAIGN, "_cookie")
    xs.LOGGER.info("Saving cookies in {}...".format(fname))
    # From http://stackoverflow.com/questions/15058462/how-to-save-and-load-
    # cookies-using-python-selenium-webdriver
    pickle.dump(xs.DRIVER.get_cookies(), open(fname, "wb"))
    xs.LOGGER.info("OK")


def stop(cookies):
    if cookies:
        save_cookies()
    if xs.DRIVER is not None:
        xs.LOGGER.info("Stopping Webdriver...")
        xs.DRIVER.quit()
        xs.LOGGER.info("OK")
