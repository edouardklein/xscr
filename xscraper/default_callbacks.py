"""Default callbacks
------------------------

Module authors will mostly write callback functions. Here are some default
ones, that various command line option will configure and put in the
:py:data:`__init__.CALLBACKS` list at startup.
"""

import datetime
import json
from collections import Counter
from urllib.parse import urljoin, urlparse
from bs4 import BeautifulSoup
from IPython import embed
import xscraper as xs
import time
import re
import warnings
import networkx as nx
from xscraper.graph import add_url, Seen, update_current
from xscraper.scram import dump_form_value
from threading import Thread
import questionary


def screenshot(scroll_back=True):
    """Take a screenshot of the current page

    :param scroll_back: scroll back up (to avoid site banners)
    :type scroll_back: bool
    """
    fname = "{}_screenshots/{}-{}.png".format(
        xs.CAMPAIGN, xs.TIMESTAMP, xs.DRIVER.current_url.replace("/", "_")
    )
    xs.LOGGER.info("Screenshoting {} in {}...".format(xs.DRIVER.current_url, fname))
    if scroll_back:
        # Scrolling back up to avoid ugly-ass site banners in the middle of the
        # page
        xs.DRIVER.execute_script("window.scrollTo(0,0)", "")
    xs.DRIVER.find_element_by_tag_name("body").screenshot(fname)


def get_default_pause(stop_regex, console):
    """Return a default pause function

    :param stop_form: stop if a form is detected
    :type stop_form: bool
    :param stop_regex: stop if the page source matches the regex
    :type stop_regex:
    :param console: go to the console at every iteration of the loop
    :type console: bool
    :return: CALLBACK function
    """

    def default_pause():
        """Pause the program and prompt the user (e.g. in order to let them
        fill a form)
        if there is a form in the page, or if the page content matches the
        regex, or
        if the --console option is specified"""
        # TODO: skip only one form's ID.
        if (stop_regex and stop_regex.search(xs.DRIVER.page_source)) or console:
            embed(header="Console for manual utilization", user_ns=globals())
            xs.CURRENT_SOUP = BeautifulSoup(xs.DRIVER.page_source, "lxml")

    return default_pause


def extract_css_attribute(source=None, _type=None):
    """
    Create a dictionnary with html attributes from a WebElement
    :param source: HTML Element
    :type source: WebElement
    :param _type: Type of the HTML element. Currently, only <input> and <button> are tolerated
    :type _type: string
    :return: dict of attributes
    """

    raw_data = source.get_attribute("outerHTML")
    if _type == "input":
        attrs = BeautifulSoup(raw_data, "lxml").input.attrs
    elif _type == "button":
        attrs = BeautifulSoup(raw_data, "lxml").button.attrs
    else:
        raise NameError("CSS type not recognized")
    return attrs


def generate_css_selector(attribute=None, _type=None):
    """
    Generate a CSS selector with all attributes from a HTML element
    :param attribute: Attributes of the element
    :type attribute: dict
    :param _type: Type of the HTML element. Currently, only <input> and <button> are tolerated
    :type _type: string
    :return: css_selector (string)
    """

    if _type == "input":
        css_selector = "input"
    elif _type == "button":
        css_selector = "button"
    else:
        raise NameError("CSS type not recognized")

    for _attribute in attribute:
        if type(attribute[_attribute]) is str:
            css_selector += f"""[{_attribute}="{attribute[_attribute]}"]"""
        # If multiple value for an attribute (class for example),
        # We must normalize the data structure according to [attribute="class1 class2"] model
        elif type(attribute[_attribute]) is list:
            normalized_attribut = """"""
            for elmt in attribute[_attribute]:
                normalized_attribut += f"""{elmt} """
            css_selector += f"""[{_attribute}="{normalized_attribut[:-1]}"]"""
    # We ignore hidden objects because humans can't see them on the browser
    # Necessary to prevent some countermeasures
    css_selector += """:not([type="hidden"])"""

    return css_selector


def interactive_form(auto_form=None, form_url=None, security=True):
    """
    Main function for automatizing form filling
    :param auto_form: Flag to define if xscraper uses or not the default value from the element informations
    :type auto_form: Bool
    :param form_url: URL where the form is detected
    :type form_url: string
    :return: None
    """

    def check_form_fill(_state_fill=None, type=None):
        while xs.DRIVER.current_url == form_url and _state_fill and security:
            check_form = questionary.confirm(
                """The URL has not changed after sending the form.
                An error may have occured (e.g. with a captcha).
                Has the form been successfully filled ?""",
                default=False,
            ).ask()
            if check_form:
                break
            else:
                xs.DRIVER.refresh()
                if type == "button":
                    _state_fill = interaction_input(auto_form)
                    _state_fill = interaction_button(auto_form)
                    check_form_fill(_state_fill=state_fill, type="button")
                if type == "form":
                    _state_fill = interaction_input(auto_form)
                    check_form_fill(_state_fill=state_fill, type="form")

    # Control the validity of the CSS selector collection
    check_duplicates()

    # Fill input field/button and check if filling is good
    state_fill = interaction_input(auto_form)
    check_form_fill(_state_fill=state_fill, type="form")

    # Press button and check if the comportment is good
    # No button type selection because there isn't usefull type except 'button' and 'submit'
    state_fill = interaction_button(auto_form)
    check_form_fill(_state_fill=state_fill, type="button")


def generate_css_selector_for_selection(elmnt_type=None, input_type=None):
    """
    Generate the global css selector according to autorized type of element
    :param elmnt_type: Type of the element (input or button)
    :type elmnt_type: string
    :param input_type: Sub-type of the element (specially for input element)
    :type input_type: string
    :return: css selector string
    """
    _css_selector = ""
    for css_type in input_type:
        # We ignore hidden objects because humans can't see them on the browser
        # Necessary to prevent some countermeasures
        _css_selector += f"{elmnt_type}:not([type='hidden'])[type='{css_type}'],"
    _css_selector = _css_selector[:-1]
    return _css_selector


def interaction_button(auto_form=None):
    """
    Main function for button control.
    IMPORTANT: Only one click per page. If you want realize more click on the page, get a manual approach.
    :param auto_form: Flag to define if xscraper uses or not the default value from the element informations
    :type auto_form: Bool
    :return: Flag to indicate if actions should be associated with a URL changing (bool)
    """

    # Necessary to prevent some countermeasures
    button_objects = xs.DRIVER.find_elements_by_css_selector(
        "button:not([type='hidden'])[type='button'],button:not([type='hidden'])[type='submit']"
    )
    # We store all button object in a list to differentiate unknown button from unused button.
    list_unknown_field = button_objects.copy()

    list_already_check_selector = []
    # For all button element, we search the corresponding CSS selector in the collection
    for button_object in button_objects:
        for css_selector in xs.FORM_MEMORY["items"]:
            if len(xs.DRIVER.find_elements_by_css_selector(css_selector)) > 1:
                raise ValueError(
                    f"the selector {css_selector} matches 2 or more elements. Please, use a selector more precise"
                )

            if (
                xs.DRIVER.find_elements_by_css_selector(css_selector)[0]
                == button_object
                if xs.DRIVER.find_elements_by_css_selector(css_selector)
                else None
            ):
                list_already_check_selector.append(css_selector)
                cntrl_correctly_filled = auto_click_button(
                    css_selector=css_selector, auto_form=auto_form
                )
                list_unknown_field.remove(button_object)

                if cntrl_correctly_filled:
                    return cntrl_correctly_filled
                break

    # If there are elements not configured, we configure them
    for _object in list_unknown_field:
        elmnt_data = configure_new_element(
            elmnt=_object,
            elmt_type="button",
            type_action=["button", "submit"],
            default_type_action="button",
        )
        # If False, the element is ignored
        if not elmnt_data:
            continue

        cntrl_correctly_filled = click_button(elmnt_data)
        if cntrl_correctly_filled:
            return cntrl_correctly_filled


def auto_click_button(css_selector=None, auto_form=None):
    """
    Function to automatically click on a known element
    :param css_selector: CSS selector of the element
    :type css_selector: string
    :param auto_form: Flag to define if xscraper uses or not the default value from the element informations
    :type auto_form: bool
    :return: None
    """
    if xs.FORM_MEMORY["items"][css_selector]["ignore"]:
        print("ACTION:RECOGNIZED BUTTON")
        print("TARGET: " + css_selector)
        print("ACTION: " + "#IGNORED#")

    if auto_form and (
        xs.DRIVER.current_url in xs.FORM_MEMORY["items"][css_selector]["url"]
        or "all" in xs.FORM_MEMORY["items"][css_selector]["url"]
    ):
        data_button = xs.FORM_MEMORY["items"][css_selector]["auto_value"]
        print("ACTION: RECOGNIZED BUTTON")
        print("TARGET: " + css_selector)
        print("ACTION: " + f"Auto-click: {data_button}")

        if data_button == "yes":
            xs.DRIVER.find_element_by_css_selector(css_selector).click()
            return True

    if not auto_form and (
        xs.DRIVER.current_url in xs.FORM_MEMORY["items"][css_selector]["url"]
        or "all" in xs.FORM_MEMORY["items"][css_selector]["url"]
    ):
        print("ACTION: RECOGNIZED BUTTON")
        print("TARGET: " + css_selector)
        use_or_not = questionary.confirm(
            "Do you want to use an already existing value ?"
        ).ask()
        if not use_or_not:
            click_or_not = questionary.confirm("Click on the button ?").ask()

            auto_or_not = questionary.confirm(
                "Do you want to use this value to autofill ?"
            ).ask()

            xs.FORM_MEMORY["items"][css_selector]["values"].append(click_or_not)
            if auto_or_not:
                xs.FORM_MEMORY["items"][css_selector]["auto_value"] = click_or_not
            dump_form_value()
        else:
            xs.LOGGER.warning(
                f"Stored value: {xs.FORM_MEMORY['items'][css_selector]['values']}"
            )
            data_button = list_checkboxQuestion(
                type="list",
                id_question="data_button",
                message="Select the correct action",
                choices=xs.FORM_MEMORY["items"][css_selector]["values"],
            )

            if data_button:
                xs.DRIVER.find_element_by_css_selector(css_selector).click()
                return True


def auto_fill_input(css_selector=None, auto_form=None):
    """
    Function to automatically fill a known element
    :param css_selector: CSS selector of the element
    :type css_selector: string
    :param auto_form: Flag to define if xscraper uses or not the default value from the element informations
    :type auto_form: bool
    :return: None
    """

    if xs.FORM_MEMORY["items"][css_selector]["ignore"]:
        print("ACTION: RECOGNIZED INPUT")
        print("TARGET: " + css_selector)
        print("ACTION: #IGNORED#")
        xs.DRIVER.find_element_by_css_selector(css_selector).clear()

    if (
        xs.DRIVER.current_url in xs.FORM_MEMORY["items"][css_selector]["url"]
        or "all" in xs.FORM_MEMORY["items"][css_selector]["url"]
    ):
        if auto_form and not xs.FORM_MEMORY["items"][css_selector]["captcha"]:
            data_input = xs.FORM_MEMORY["items"][css_selector]["auto_value"]
            print("ACTION: RECOGNIZED INPUT")
            print("TARGET: " + css_selector)
            print("ACTION: " + f"Auto-click: {data_input}")
            xs.DRIVER.find_element_by_css_selector(css_selector).clear()
            xs.DRIVER.find_element_by_css_selector(css_selector).send_keys(data_input)

        if not auto_form and not xs.FORM_MEMORY["items"][css_selector]["captcha"]:
            print("ACTION: RECOGNIZED INPUT")
            print("TARGET: " + css_selector)

            use_or_not = questionary.confirm(
                "Do you want to use an already existing value ?"
            )
            if not use_or_not:

                data_input = questionary.text(
                    f"""Fill the field (Keep empty if no interest)
                type: {xs.DRIVER.find_element_by_css_selector(css_selector).get_attribute('type')}
                """
                ).ask()

                xs.FORM_MEMORY["items"][css_selector]["values"].append(data_input)
                auto_or_not = questionary.confirm(
                    "Do you want to use this value in autofill ?"
                )
                if auto_or_not:
                    xs.FORM_MEMORY["items"][css_selector]["auto_value"] = data_input
                dump_form_value()
            else:
                data_input = list_checkboxQuestion(
                    type="list",
                    id_question="data_input",
                    message="Select the id of the correct value to fill",
                    choices=xs.FORM_MEMORY["items"][css_selector]["values"],
                )

            xs.DRIVER.find_element_by_css_selector(css_selector).clear()
            xs.DRIVER.find_element_by_css_selector(css_selector).send_keys(data_input)

        if xs.FORM_MEMORY["items"][css_selector]["captcha"]:
            print("ACTION: RECOGNIZED BLOCKING CAPTCHA")
            print("TARGET: " + css_selector)
            data_input = questionary.text("Fill the captcha: ").ask()
            xs.DRIVER.find_element_by_css_selector(css_selector).clear()
            xs.DRIVER.find_element_by_css_selector(css_selector).send_keys(data_input)


def click_button(elmnt_data=None):
    """
    Click or not on a specific button
    :param elmnt_data: HTML element
    :type elmnt_data: WebElement
    :return: None
    """
    click_or_not = questionary.confirm("Click on the button ?").ask()
    if click_or_not:
        xs.FORM_MEMORY["items"][elmnt_data[2]]["auto_value"] = "yes"
        xs.FORM_MEMORY["items"][elmnt_data[2]]["values"].append("yes")
        xs.DRIVER.find_element_by_css_selector(elmnt_data[2]).click()
        dump_form_value()
        return True
    else:
        xs.FORM_MEMORY["items"][elmnt_data[2]]["auto_value"] = "no"
        xs.FORM_MEMORY["items"][elmnt_data[2]]["values"].append("no")
        dump_form_value()


def check_duplicates():
    """
    Check if some css selector in the config file aim the same element or if a css selector aims more than one element.
    If True, xscraper will be stopped.
    :return: None
    """
    list_field_detected = (
        [
            xs.DRIVER.find_elements_by_css_selector(css_selector)[0]
            if (
                xs.DRIVER.find_elements_by_css_selector(css_selector)
                and len(xs.DRIVER.find_elements_by_css_selector(css_selector)) < 2
            )
            else None
            for css_selector in xs.FORM_MEMORY["items"]
        ]
        if xs.FORM_MEMORY
        else []
    )

    duplicate_test = [
        True if Counter(list_field_detected)[idx] > 1 else False
        for idx in Counter(list_field_detected)
        if idx is not None
    ]

    if True in duplicate_test:
        raise ValueError(
            "Some css selector in the JSON file aim the same element. Please, remove duplicates"
        )


def list_checkboxQuestion(type=None, id_question=None, message=None, choices=None):
    """
    Function used if you want ask a multiple-choice question to the user (checkbox or list)
    :param type: Type of the question
    :type type: string ("list" or "checkbox")
    :param id_question: Id of the question
    :type id_question: string
    :param message: Text of the question
    :type message: string
    :param choices: Universe of the possibility
    :type choices: list
    :return: answser of the user (string or list)
    """
    questions = [
        {
            "type": type,
            "name": id_question,
            "message": message,
            "choices": [questionary.Separator("= Select the values =")]
            + [{"name": values} for values in choices],
            "validate": lambda answer: "You must choose at least one topping."
            if len(answer) == 0
            else True,
        }
    ]

    return questionary.prompt(questions)[id_question]


def interaction_input(auto_form=None):
    """
    Main function for input interaction
    :param auto_form: Flag to define the mode. True will activate automatic approach, False, a semi-supervised approach
    :type auto_form: Bool
    :return: Flag to indicate if actions should be associated with a URL changing (bool)
    """

    # Generate an automatic css selector to isolate specific types of HTML input
    if not xs.FORM_MEMORY["data_type_input"]:
        _input_type = list_checkboxQuestion(
            type="checkbox",
            id_question="data_type",
            message="Select all input types that you want consider",
            choices=["text", "password", "number", "submit", "search"],
        )

        xs.FORM_MEMORY["data_type_input"] = generate_css_selector_for_selection(
            "input", _input_type
        )

    # Get all input objects according to their type
    input_objects = xs.DRIVER.find_elements_by_css_selector(
        xs.FORM_MEMORY["data_type_input"]
    )
    list_unknown_field = input_objects.copy()
    list_already_check_selector = []

    for input_object in input_objects:
        attrs = extract_css_attribute(input_object, "input")
        _type_action = attrs["type"] if "type" in attrs else "text"
        for css_selector in xs.FORM_MEMORY["items"]:
            if len(xs.DRIVER.find_elements_by_css_selector(css_selector)) > 1:
                raise ValueError(
                    f"The selector {css_selector} matches 2 or more elements. Please, use a selector more precise"
                )

            if css_selector in list_already_check_selector:
                continue

            if (
                xs.DRIVER.find_elements_by_css_selector(css_selector)[0] == input_object
                if xs.DRIVER.find_elements_by_css_selector(css_selector)
                else None
            ):
                list_already_check_selector.append(css_selector)

                if _type_action in ["text", "password", "number", "search"]:
                    auto_fill_input(css_selector=css_selector, auto_form=auto_form)
                    list_unknown_field.remove(input_object)
                    break

                if _type_action == "submit":
                    cntrl_correctly_filled = auto_click_button(
                        css_selector=css_selector, auto_form=auto_form
                    )
                    list_unknown_field.remove(input_object)
                    if cntrl_correctly_filled:
                        return cntrl_correctly_filled

    for _object in list_unknown_field:
        elmnt_data = configure_new_element(
            elmnt=_object,
            elmt_type="input",
            type_action=["text", "submit", "password", "number", "search"],
            default_type_action="text",
        )

        # If False, the element is ignored
        if not elmnt_data:
            continue

        # https://www.w3schools.com/tags/att_input_type.asp
        # A input field can be a button too. We must differentiate a button from a text field
        # If field:
        if elmnt_data[0] in ["text", "password", "number", "search"]:
            xs.FORM_MEMORY["items"][elmnt_data[2]]["type"] = elmnt_data[1]["type"]
            captcha_or_not = questionary.confirm(
                "Is it a blocking captcha ?",
                default=False,
            ).ask()

            data_input = questionary.text(
                "Fill the field (Keep empty if no interest) type: {_object.get_attribute('type')}:"
            ).ask()

            if captcha_or_not:
                xs.FORM_MEMORY["items"][elmnt_data[2]]["captcha"] = True
            else:
                xs.FORM_MEMORY["items"][elmnt_data[2]]["auto_value"] = data_input
                xs.FORM_MEMORY["items"][elmnt_data[2]]["values"].append(data_input)

            dump_form_value()
            xs.DRIVER.find_element_by_css_selector(elmnt_data[2]).clear()
            xs.DRIVER.find_element_by_css_selector(elmnt_data[2]).send_keys(data_input)
            continue

        # If button
        elif elmnt_data[0] == "submit":
            cntrl_correctly_filled = click_button(elmnt_data)
            if cntrl_correctly_filled:
                return cntrl_correctly_filled

        # If other things:
        else:
            xs.LOGGER.warning(
                f"Currently, the {elmnt_data[0]} css type is not supported for {elmnt_data[1]['type']} HTML element. "
                f"This element will be ignored"
            )


def configure_new_element(
    elmnt, elmt_type=None, type_action=None, default_type_action=None
):
    """
    Function called to define HTML element data according to user informations
    :param elmnt: Element from the HTML
    :type elmnt: WebElement
    :param elmt_type: Type of the element (input or button)
    :type elmt_type: string
    :param type_action: Autorized action types of the element (text, submit, button, ...)
    :type type_action: list
    :param default_type_action: Action type by default if no type is indicated
    :type default_type_action: string
    :return: list[action type (string), attributes (dict), css selector (string)]
    """
    attrs = extract_css_attribute(elmnt, elmt_type)
    _type_action = attrs["type"] if "type" in attrs else default_type_action

    # Not supported element type
    if _type_action not in type_action:
        print("ACTION: " + f"NEW {elmt_type}")
        print("ATTRIBUTE: " + attrs)
        xs.LOGGER.warning(
            f"Type {attrs['type']} is not supported currently. xscraper will ignore this element !"
        )
        return False

    # Generated a css selector for the element
    css_selector = generate_css_selector(attrs, elmt_type)
    print("ACTION: " + f"NEW {elmt_type}")
    print("ATTRIBUTE: " + attrs)

    # Keep the generated css selector or customize it
    css_selector = questionary.text(
        message="A css selector has been automatically created. Modify it if you want. "
        "BE SURE that your custom css selector is correct ! "
        "Editing can be necessary if the selector by default is too general:\n ",
        default=css_selector,
    ).ask()

    # A css selector must be unique
    if css_selector not in xs.FORM_MEMORY["items"]:
        xs.FORM_MEMORY["items"][css_selector] = {
            "ignore": False,
            "captcha": False,
            "auto_value": None,
            "values": [],
            "url": [],
        }
    else:
        while True:
            xs.LOGGER.warning(
                "Css selector already exists. Please, use an another one."
            )
            css_selector = questionary.text(
                message="Css selector already exists. Please, use an another one: ",
            ).ask()
            if css_selector not in xs.FORM_MEMORY["items"]:
                xs.FORM_MEMORY["items"][css_selector] = {
                    "ignore": False,
                    "captcha": False,
                    "auto_value": None,
                    "values": [],
                    "url": [],
                }
                break

    # Consider the element or not
    ignore_or_not = questionary.confirm("Do you want ignore this element ?").ask()
    if ignore_or_not:
        xs.FORM_MEMORY["items"][css_selector]["ignore"] = True
        dump_form_value()
        return False

    # Do the specified action on all pages or not
    all_or_not = questionary.confirm(
        "Use this configuration for all page ?",
    ).ask()
    if not all_or_not:
        xs.FORM_MEMORY["items"][css_selector]["url"].append(xs.DRIVER.current_url)
    else:
        xs.FORM_MEMORY["items"][css_selector]["url"].append("all")

    return [_type_action, attrs, css_selector]


class auto_log_page:
    def __init__(self, max_page=None, max_time=None):
        """
        Automatic login/logout during the crawling when a particular event is detected (number of visited page or time since the last logout)
        :param max_page: Number of visited pages before a logout/login
        :param max_time: Time before a logout/login
        """

        self.max_page = int(max_page)
        self.max_time = int(max_time)
        self.counter_page = 0  # it doesn't exist yet, so initialize it
        self.counter_last_refresh = time.time()

    def execute(self):
        form_url_src = xs.DRIVER.current_url
        if (self.max_page and self.counter_page >= self.max_page) or (
            self.max_time and time.time() - self.counter_last_refresh >= self.max_time
        ):
            xs.LOGGER.warning("Login refresh ! Automatic logout now !")

            for value in xs.FORM_MEMORY["login"]["logout_selector"]:
                if xs.DRIVER.find_elements_by_css_selector(value):
                    xs.DRIVER.find_element_by_css_selector(value).click()
                else:
                    xs.LOGGER.warning(
                        "Login refresh FAILED ! CSS selector not recognized"
                    )
                    break

            tor_daemux = [p for p in xs.PROGRAMS if p.cmd.startswith("tor")][0]
            xs.LOGGER.info("Restart Tor...")
            tor_daemux.restart()
            # tor_daemux.stop()
            # tor_daemux.start()
            # # Why the % symbol (Bootstrapped 100%: Done) crashes the command ?
            # tor_daemux.wait_for_output('Bootstrapped 100', timeout=20)
            # xs.LOGGER.info("OK")

            self.counter_page = 0
            self.counter_last_refresh = time.time()

        if xs.DRIVER.find_elements_by_css_selector(
            xs.FORM_MEMORY["login"]["login_selector"]
        ):
            xs.LOGGER.warning("Login process ! Automatic login now !")
            # We store the URL and change its statut as SEEN
            form_url = xs.DRIVER.current_url
            if form_url in xs.G.nodes:
                xs.G.nodes[form_url]["seen"] = Seen.SEEN
            else:
                xs.graph.add_url(
                    url=form_url, parent_url=form_url_src, seen=xs.graph.Seen.SEEN
                )

            interactive_form(True, form_url, False)

            # We update the HTML due to the new page and add the new page to the graph
            xs.graph.update_current()
            if xs.DRIVER.current_url != form_url:
                add_url(xs.DRIVER.current_url, form_url)

        self.counter_page += 1


def get_form_pause(
    interaction="manual", dict_form=None, auto_form=None, _screenshot=None
):
    """
    Return a function that interacts with forms in page. Forms can be automatically filled or filled by human interaction.
    This function is activated if a form is detected on the page.
    :param interaction: type of form interaction
    :type interaction: str
    :param dict_form: path of a json file which contains form data
    :type dict_form: str
    :param auto_form: Flag to indicate if xscraper will use by default values
    :type auto_form: bool
    :param _screenshot: flag to indicate if xscraper will realize a screenshot of the visited page
    :type _screenshot: bool
    :return: CALLBACK function
    """
    # PLACER DANS LE CLI
    if dict_form:
        with open(dict_form, "r") as f:
            xs.FORM_MEMORY = json.load(f)
    else:
        xs.FORM_MEMORY = {"items": {}, "data_type_input": None}

    def get_form_pause():
        """ """
        # Check if there is any form in the page
        if xs.CURRENT_SOUP.form:
            # We store the URL and change its statut as SEEN
            form_url = xs.DRIVER.current_url
            xs.G.nodes[form_url]["seen"] = Seen.SEEN

            if interaction == "manual":
                console()
            elif interaction == "interactive":
                interactive_form(auto_form, form_url)
            else:
                raise ValueError(str(interaction) + " is a unknow parameter")

            # We update the HTML due to the new page and add the new page to the graph
            xs.graph.update_current()
            if xs.DRIVER.current_url != form_url:
                add_url(xs.DRIVER.current_url, form_url)
                if _screenshot:
                    screenshot()

    return get_form_pause


def get_scroll_down(s):
    """Return a function that scrolls down for the specified duration

    :param s: time of scrolling ( in second )
    :type s: int
    :return: CALLBACK function
    """

    def scroll_down():
        """Scroll down"""
        xs.LOGGER.info("Scrolling down on %s" % xs.DRIVER.current_url)
        t1 = datetime.datetime.now()
        while datetime.datetime.now() - t1 < datetime.timedelta(seconds=s):
            # 5000 valeur arbitrairement grande
            xs.DRIVER.execute_script("window.scrollBy(0,5000)", "")
            xs.LOGGER.info("scrolling...")
            time.sleep(0.2)
            update_current()

    return scroll_down


def infinite_scroll(log=False):
    """Scroll down, sleep, up until the end of the page.

    This function is useful to get to the bottom of the infinite scroll
    pages (e.g. Facebook)"""
    while True:
        before_scroll = xs.DRIVER.execute_script(
            "return document." "documentElement.scrollTop"
        )
        xs.LOGGER.info("Scrolling from {}...".format(before_scroll)) if log else None
        xs.DRIVER.execute_script(
            """var h;
            do{
                h = document.documentElement.scrollTop;
                window.scrollBy(0,5000)
            }while(h !=document.documentElement.scrollTop)"""
        )
        time.sleep(4)
        xs.LOGGER.info("...") if log else None
        after_scroll = xs.DRIVER.execute_script(
            "return document." "documentElement.scrollTop"
        )
        xs.LOGGER.info("... to {}".format(after_scroll)) if log else None
        if after_scroll == before_scroll:
            xs.LOGGER.info("Reached the bottom of the page") if log else None
            update_current()
            return


def get_default_crawler(depth, black_regex, white_regex, black_dominate, domain=None):
    """
    Return the default crawler

    :param depth: depth of the scraping (min distance between current node and
        a root node)
    :type depth: int
    :param black_regex: Do not scrap any url that matches this regex
    :type black_regex: _sre.SRE_Pattern
    :param white_regex: Only scrap urls that match this regex
    :type white_regex: _sre.SRE_Pattern
    :param black_dominate: Indicate if blacklist tag dominate the whitelist tag
    :type black_dominate: bool
    :param domain: Do not scrap any url outside of this domain
    :type domain: string
    :return: CALLBACK function

    If a URL matches both the black and the white regex, it will be scraped
    (the white list has priority).
    """

    def default_crawler():
        """A simple crawler that will add a URL to the TO_BE_SEEN FIFO if
        it is not in the black list and if it not absent from the white list
        and
        if its depth is below the minimal depth and if its domain is the
        scraped domain"""

        for link in [l for l in xs.CURRENT_SOUP.find_all("a") if "href" in l.attrs]:
            link = urljoin(xs.DRIVER.current_url, link["href"])
            seen_value = xs.graph.Seen.YET_TO_BE_SEEN
            parsed = urlparse(link)

            # Domain checking ; if url has other domain that url gived in line
            # command, a node will be created but the url will not be visited
            if domain:
                if not parsed.netloc.endswith(domain):
                    seen_value = xs.graph.Seen.NOT_TO_BE_SEEN
            # Url in black list will not be visited but listed
            if black_regex and black_regex.search(link):
                seen_value = xs.graph.Seen.NOT_TO_BE_SEEN
            # Only url in white list will be visited
            if white_regex and not white_regex.search(link):
                seen_value = xs.graph.Seen.NOT_TO_BE_SEEN
            # If white and black flag are raise ?
            if (
                white_regex
                and white_regex.search(link)
                and black_regex
                and black_regex.search(link)
            ):
                seen_value = (
                    xs.graph.Seen.NOT_TO_BE_SEEN
                    if black_dominate
                    else xs.graph.Seen.YET_TO_BE_SEEN
                )

            if xs.DRIVER.current_url not in xs.G.nodes:
                raise KeyError(
                    xs.DRIVER.current_url
                    + " is not known by the graph. Do you have used auto_add_page plugin "
                    "correctly (no human interaction with the browser before the complete "
                    "initialization of the plugin) ?"
                )

            add_url(url=link, parent_url=xs.DRIVER.current_url, seen=seen_value)

            # Depth checking
            if (
                depth
                and nx.shortest_path_length(
                    xs.G,
                    source=[
                        url for url in xs.G.nodes if xs.G.nodes[url]["root"] is True
                    ][0],
                    target=xs.DRIVER.current_url,
                )
                > depth
            ):
                xs.G.nodes[link]["seen"] = xs.graph.Seen.NOT_TO_BE_SEEN
                xs.LOGGER.info(link + " : URL refused because too high depth")

    return default_crawler


def console(header="Use ctrl-D to quit the console"):
    """Drop the user to an interactive prompt"""
    embed(header=header, user_ns=globals())


def console_once(header="Use CTRL-D to quit the console"):
    """Self-removing callback activating the console

    The header can be changed from the default one."""
    with xs.LOCK:
        del xs.CALLBACKS[xs.CALLBACKS_INDEX]
    # We delete a Callbacks so the length of the list is changed
    # The main loop increments the index and uses the length of the Callbacks list
    # as reference. Without substraction of 1, next callback function will not be called
    # because len(callbacks) = 1 and index too so the loop is over.
    xs.CALLBACKS_INDEX -= 1
    console(header=header)


class DetectNewPage(Thread):
    def __init__(
        self,
        source_url_state=Seen.SEEN,
        destination_url_state=Seen.YET_TO_BE_SEEN,
        extract_url_or_not=False,
        arguments=None,
    ):
        """
        Thread to control adding of new pages according to a manual crawling of the website
        :param source_url_state: State given to the source page
        :param destination_url_state: State given to the destination page
        :param extract_url_or_not: On each visited page, extract or not all detected Urls
        :param arguments: arguments from the xscraper' session
        """
        Thread.__init__(self)
        self.source_url_state = source_url_state
        self.destination_url_state = destination_url_state
        self.running = True
        self.arguments = arguments
        self.extract_url_or_not = extract_url_or_not
        self.black_regex = (
            re.compile(arguments["--except"]) if arguments["--except"] else None
        )
        self.white_regex = (
            re.compile(arguments["--only"]) if arguments["--only"] else None
        )
        if arguments["--depth"]:
            self.depth = int(arguments["--depth"])
        else:
            self.depth = None

    def page_analysis(self):
        """
        Execute an analysis of the HTML code on all visited page. The function can be from an extern script file
        """
        if self.arguments["--aap_page_analysis"]:
            try:
                xs.LOGGER.warning(
                    "Page analysis in progress... Please, don't change the page until the analysis is done !"
                )
                getattr(
                    self.arguments["--module"], self.arguments["--aap_page_analysis"]
                )()
            except AttributeError:
                warninxs.warn(
                    f"Imported page analysis function '{self.arguments['--aap_page_analysis']}' not found. Please, verify if your function has been already imported in the xscraper instance (by plugin import or manually in the console). The analysis task is ABORTED."
                )

        if self.extract_url_or_not:
            xs.LOGGER.warning(
                "Page analysis in progress... Please, don't change the page until the analysis is done !"
            )
            get_default_crawler(
                self.depth,
                self.black_regex,
                self.white_regex,
                self.arguments["--except_first"],
                xs.CAMPAIGN if not self.arguments["--no_domain_filter"] else None,
            )()
            xs.LOGGER.info("Urls extraction done !")

        xs.LOGGER.warning("The page analysis is done !")

    def run(self):
        """ Launch the Thread """

        # The auto_mode is only activated one time by the Callbacks system
        with xs.LOCK:
            del xs.CALLBACKS[xs.CALLBACKS_INDEX]
            xs.CALLBACKS_INDEX -= 1

        form_url = xs.DRIVER.current_url
        print("Automatic detection of pages: ACTIVATED")
        print("Destination URLs: " + self.destination_url_state)
        print("Source URLs: " + self.source_url_state)

        if self.arguments["--screenshot"]:
            screenshot()
            xs.LOGGER.info("Screenshot done !")

        self.page_analysis()
        print("Waiting for actions...")

        while self.running:
            if xs.DRIVER.current_url == form_url:
                pass
            else:
                with xs.LOCK:
                    try:
                        xs.G.nodes[form_url]["seen"] = self.source_url_state
                    except KeyError:
                        xs.LOGGER.warning(
                            "\nWARNING: The source node is not defined in the graph. \n"
                            "You have probably interacted with the browser before the launching of the "
                            "auto-add protocol. Be sure it is ready before interacting with the browser !"
                            "\nThe unknow node will be added but the previous relations of this node are lost. "
                            "The graph can be incomplete due to this phenomenon."
                        )
                        add_url(url=form_url, seen=self.source_url_state)
                    xs.graph.update_current()

                    if self.arguments["--screenshot"]:
                        screenshot()
                        xs.LOGGER.info("Screenshot done !")

                    add_url(
                        xs.DRIVER.current_url, form_url, seen=self.destination_url_state
                    )
                    xs.LOGGER.info("\nNew page detected !")
                    xs.LOGGER.info(f"Create new node for {xs.DRIVER.current_url}")
                    xs.LOGGER.info(
                        f"Create link between {xs.DRIVER.current_url} and {form_url}\n"
                    )

                    self.page_analysis()

                form_url = xs.DRIVER.current_url
                xs.LOGGER.info("Waiting for actions...")
        print("Automatic detection of pages: DESACTIVATED")

    def stop(self):
        """ Stop the Thread """
        self.running = False


def auto_add_page(arguments=None):
    """
    Function to automatically add new pages according to a manual crawling of the website. This function permits an
    interaction with a python terminal in the same time. To come back to the automatic way, you just quit the python terminal.
    :return: None
    """
    print("Automatic detection of pages: INITIALIZATION")
    xs.LOGGER.warning("source URL ---> click ---> destination URL")

    source_url_state = questionary.confirm(
        "Do you want source URLs to be considered as already seen ?",
    ).ask()
    source_url_state = Seen.SEEN if source_url_state else Seen.YET_TO_BE_SEEN

    destination_url_state = questionary.confirm(
        "Do you want destination URLs to be considered as already seen ?"
    ).ask()
    destination_url_state = (
        Seen.YET_TO_BE_SEEN if not destination_url_state else Seen.SEEN
    )

    extract_url_or_not = False
    if not arguments["--aap_page_analysis"]:
        extract_url_or_not = questionary.confirm(
            "Do you want extract Urls from visited pages ?"
        ).ask()

    thread = DetectNewPage(
        source_url_state, destination_url_state, extract_url_or_not, arguments
    )
    thread.start()
    console(
        f"You can execute actions during the automatic pages adding.\n"
        f"The page analysis starts when the COMPLETE LOADING of the page is done\n"
        f"If you want come back to the automatic way, get out of the python terminal (CNTRL-D)\n"
    )
    # Stop properly the thread
    thread.stop()
    # To be sure the thread can't work in the same time as the automatic core of xscraper
    thread.join()
