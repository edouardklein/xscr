all: test doc xscraper.tar.gz

test:
	make -C tests clean
	make -C tests

xscraper.tar.gz:
	cp $$(guix pack -L . -e '(@@ (osef) xscraper)') xscraper.tar.gz

doc:
	make -C docs html

clean:
	make -C docs clean
	rm -rf xscraper/__pycache__
	rm -rf xscraper.egg-info/
	rm -rf build/
	rm -f *::localhost:8101.json  # Created by the tests

cleanall: clean
	@echo -n "Are you FUCKING sure? " && read ans && [ $$ans == y ]
	rm -f *.log
	rm -f *.json
	rm -f *.replay

kill:
	-a=$$(tmux list-session | cut -d":" -f1 | grep xscraper) && tmux kill-session -t $$a
	-tmux kill-session -t Replay
	-pkill polipo
	-pkill chromedriver
	-pkill mitmdump
	-pkill python3 --signal 9
	-pkill tor

tuto:   doc
	make -C tests http_server.pid
	pipenv run python ./xscraperGui.py &
	chromium --no-sandbox --incognito file://`pwd`/docs/_build/html/index.html &
	lxterminal --command ./docs/tuto.sh
